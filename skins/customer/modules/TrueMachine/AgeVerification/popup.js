var AgeVerification = {
    init: function (setting) {
        var self = this;
        console.log('setting lock', setting);
        if (setting) {
            AgeVerification.config = {
                getAgeVerification: setting.getAgeVerification,
                getPopupIsEnabled: setting.getPopupIsEnabled,
                isAdmin: setting.isAdmin,
                getWidth: setting.getWidth,
                getHeight: setting.getHeight,
                getBackgroundColor: setting.getBackgroundColor,
                getBackgroundOpacity: setting.getBackgroundOpacity,
                getTitle: setting.getTitle,
                getDescription: setting.getDescription,
                getMinAge: setting.getMinAge,
                getDenyMessage: setting.getDenyMessage,
                getPopupType: setting.getPopupType,
                getAwayButtonText: setting.getAwayButtonText,
                getSuccessButtonText: setting.getSuccessButtonText,
                getFailureButtonText: setting.getFailureButtonText,
            };
        }
        AgeVerification.elements = {
            ageVerificationPopup: document.getElementById('ageVerificationPopup'),
            ageVerificationPopupContent: document.getElementById('age-verification-popup-content'),
            ageVerificationPopupContentTitle: document.getElementById('age-verification-popup-content-title'),
            ageVerificationPopupContentLabelText: document.getElementById('age-verification-popup-content-label-text'),
            ageVerificationPopupContentDescription: document.getElementById('age-verification-popup-content-description'),
            ageVerificationPopupContentContainerSettings: document.getElementById('age-verification-popup-content-container-settings'),
            ageVerificationPopupContentSettingsAge: document.getElementById('age-verification-popup-content-settings-age'),
            ageVerificationPopupContentContainerNoSettings: document.getElementById('age-verification-popup-content-container-no-settings'),
            ageVerificationPopupContentSettingsBtnAway: document.getElementById('age-verification-popup-content-settings-btn-away'),
            ageVerificationPopupContentSettingsContainerInput: document.getElementById('age-verification-popup-content-settings-container-input'),
            ageVerificationPopupContentSettingsBtnSuccess: document.getElementById('age-verification-popup-content-settings-btn-success'),
            ageVerificationPopupContentNoSettingsBtnAway: document.getElementById('age-verification-popup-content-no-settings-btn-away'),
            ageVerificationPopupContentNoSettingsBtnFailure: document.getElementById('age-verification-popup-content-no-settings-btn-failure'),
            ageVerificationPopupContentNoSettingsBtnSuccess: document.getElementById('age-verification-popup-content-no-settings-btn-success')
        };
        this.render();

        AgeVerification.elements.ageVerificationPopupContentNoSettingsBtnSuccess.onclick = function () {
            console.log('CLICK NO')
            core.post(
                URLHandler.buildURL({
                    target: 'age_verification',
                    action: 'send'
                }), function () {

                }, {
                    'btn_success': true,
                })
                .done(function (data) {
                    self.init(data);
                });
        }

        AgeVerification.elements.ageVerificationPopupContentSettingsBtnSuccess.onclick = function () {
            console.log('CLICK');
            core.post(
                URLHandler.buildURL({
                    target: 'age_verification',
                    action: 'send'
                }), function () {

                }, {
                    'btn_success': true,
                    'age': AgeVerification.elements.ageVerificationPopupContentSettingsAge.value
                })
                .done(function (data) {
                    self.init(data);
                    console.log('data', data);
                });
        }

        AgeVerification.elements.ageVerificationPopupContentNoSettingsBtnFailure.onclick = function () {
            console.log('btnfailure');
            core.get(
                URLHandler.buildURL({
                    target: 'age_verification',
                    action: 'send'
                }), function () {

                }, {
                    'btn_failure': true,
                })
                .done(function (data) {
                    self.init(data);
                    console.log('setting', setting);
                    console.log('data', data)
                });
        };

        AgeVerification.elements.ageVerificationPopupContentNoSettingsBtnAway.onclick = function () {
            console.log('btnaway')
            core.get(
                URLHandler.buildURL({
                    target: 'age_verification',
                    action: 'send'
                }), function () {

                }, {
                    'btn_away': true,
                })
                .done(function (data) {
                    self.init(data);
                });
        }

    },

    render: function () {
        if (!AgeVerification.config) {
            return;
        }
        if (AgeVerification.config.getAgeVerification != 1 && AgeVerification.config.getPopupIsEnabled &&
            !AgeVerification.config.isAdmin) {
            AgeVerification.elements.ageVerificationPopup.style.display = 'block';
        } else {
            AgeVerification.elements.ageVerificationPopup.style.display = 'none';
        }

        AgeVerification.elements.ageVerificationPopup.style.backgroundColor = 'rgba(0,0,0,' + AgeVerification.config.getBackgroundOpacity + ')';
        AgeVerification.elements.ageVerificationPopupContent.style.width = AgeVerification.config.getWidth;
        AgeVerification.elements.ageVerificationPopupContent.style.width = AgeVerification.config.getHeight;
        AgeVerification.elements.ageVerificationPopupContent.style.width = AgeVerification.config.getBackgroundColor;
        AgeVerification.elements.ageVerificationPopupContentTitle.innerText = AgeVerification.config.getTitle;
        AgeVerification.elements.ageVerificationPopupContentLabelText.innerText = AgeVerification.config.getMinAge + ('+');

        if (AgeVerification.config.getAgeVerification == 0) {
            AgeVerification.elements.ageVerificationPopupContentDescription.innerText = AgeVerification.config.getDenyMessage;
        } else {
            AgeVerification.elements.ageVerificationPopupContentDescription.innerText = AgeVerification.config.getDescription;
        }

        if (AgeVerification.config.getPopupType) {
            AgeVerification.elements.ageVerificationPopupContentContainerSettings.style.display = 'block';
            AgeVerification.elements.ageVerificationPopupContentContainerNoSettings.style.display = 'none';
            if (AgeVerification.config.getAgeVerification == 0) {
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnAway.style.display = 'inline-block';
                AgeVerification.elements.ageVerificationPopupContentSettingsContainerInput.style.display = 'none';
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnSuccess.style.display = 'none';
            } else {
                AgeVerification.elements.ageVerificationPopupContentSettingsContainerInput.style.display = 'block';
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnSuccess.style.display = 'inline-block';
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnAway.style.display = 'none';
            }
        } else {
            AgeVerification.elements.ageVerificationPopupContentContainerNoSettings.style.display = 'none';
            AgeVerification.elements.ageVerificationPopupContentContainerSettings.style.display = 'block';
            if (AgeVerification.config.getAgeVerification == 0) {
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnAway.style.display = 'inline-block';
                AgeVerification.elements.ageVerificationPopupContentNoSettingsBtnFailure.style.display = 'none';
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnSuccess.style.display = 'none';
                AgeVerification.elements.ageVerificationPopupContentSettingsContainerInput.style.display = 'none';
            } else {
                AgeVerification.elements.ageVerificationPopupContentNoSettingsBtnFailure.style.display = 'inline-block';
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnSuccess.style.display = 'inline-block';
                AgeVerification.elements.ageVerificationPopupContentSettingsBtnAway.style.display = 'none';
            }
        }

    }
};
// core.get(
//     URLHandler.buildURL({
//         target: 'age_verification'
//     }))
//     .done(function (data) {
//         window.onload = AgeVerification.init(data);
//     });
window.onload = AgeVerification.init();
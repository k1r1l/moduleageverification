<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TrueMachine\AgeVerification\Controller\Customer;

use XLite\Core\Config;

/**
 * Contact us controller
 */
class AgeVerification extends \XLite\Controller\Customer\ACustomer
{
    public function doActionSend()
    {
        $urlAway = Config::getInstance()->TrueMachine->AgeVerification->a_v_url_away;
        $popupType = Config::getInstance()->TrueMachine->AgeVerification->a_v_p_type;
        $profile = $this->getProfile();

        \XLite\Core\Session::getInstance()->age_verification = !$this->isAnonymous() ?
            $profile->getAgeVerification() : null;

        if(!$popupType) {
            if(isset(\XLite\Core\Request::getInstance()->btn_failure)){
                \XLite\Core\Session::getInstance()->age_verification = '0';
                if(!is_null($profile))
                {
                    $profile->setAgeVerification(\XLite\Core\Session::getInstance()->age_verification);
                }
            } else if(isset(\XLite\Core\Request::getInstance()->btn_success)){
                \XLite\Core\Session::getInstance()->age_verification = '1';
                if(!is_null($profile))
                {
                    $profile->setAgeVerification(\XLite\Core\Session::getInstance()->age_verification);
                }
            } else if(isset(\XLite\Core\Request::getInstance()->btn_away)){
                $this->setHardRedirect(true);
                $this->setReturnURL($urlAway);
                //$this->redirect($urlAway);
            }
        } else {
            $age = (int)\XLite\Core\Request::getInstance()->age;
            $requireAge = Config::getInstance()->TrueMachine->AgeVerification->a_v_min_age;

            if($age >= $requireAge && isset(\XLite\Core\Request::getInstance()->btn_success)) {
                \XLite\Core\Session::getInstance()->age_verification = '1';
                if(!is_null($profile))
                {
                    $profile->setAgeVerification(\XLite\Core\Session::getInstance()->age_verification);
                }
            } else if($age < $requireAge && isset(\XLite\Core\Request::getInstance()->btn_success)) {
                \XLite\Core\Session::getInstance()->age_verification = '0';
                if(!is_null($profile))
                {
                    $profile->setAgeVerification(\XLite\Core\Session::getInstance()->age_verification);
                }
            } else if(isset(\XLite\Core\Request::getInstance()->btn_away)) {
                $this->redirect($urlAway);
            }
        }

        if($profile) {
            \XLite\Core\Database::getEM()->persist($profile);
            \XLite\Core\Database::getEM()->flush();
        }

        $response = $this->getResponseObject();

        $this->set('silent', true);
        $this->setSuppressOutput(true);
        $this->displayJSON($response);
    }

    public function isAnonymous()
    {
        $profile = \XLite\Core\Auth::getInstance()->getProfile();

        return $profile ? $profile->getAnonymous() : true;
    }

    public function getProfile()
    {
        return \XLite\Core\Auth::getInstance()->getProfile();
    }

    public function getResponseObject()
    {
        $profile = $this->getProfile();

        \XLite\Core\Session::getInstance()->age_verification =
            \XLite\Core\Session::getInstance()->age_verification == null ? !$this->isAnonymous() ?
                $profile->getAgeVerification() : null : \XLite\Core\Session::getInstance()->age_verification;

        $response = [
            'getAgeVerification' => \XLite\Core\Session::getInstance()->age_verification,
            'getPopupIsEnabled' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_p_enabled,
            'isAdmin' => \XLite\Core\Auth::getInstance()->isOperatingAsUserMode() ||
                \XLite\Core\Auth::getInstance()->hasRootAccess(),
            'getWidth' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_width,
            'getHeight' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_height,
            'getBackgroundColor' => '#' . \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_b_color,
            'getBackgroundOpacity' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_b_opacity * 0.01,
            'getTitle' => strip_tags(\XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_title),
            'getDescription' => strip_tags(\XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_description),
            'getMinAge' => strip_tags(\XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_min_age),
            'getDenyMessage' => strip_tags(\XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_d_message),
            'getPopupType' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_p_type,
            'getAwayButtonText' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_away_btn,
            'getSuccessButtonText' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_succ_btn,
            'getFailureButtonText' => \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_fail_btn
        ];

        return $response;
    }

    protected function doNoAction()
    {
        $response = $this->getResponseObject();

        $this->set('silent', true);
        $this->setSuppressOutput(true);
        $this->displayJSON($response);
    }
}

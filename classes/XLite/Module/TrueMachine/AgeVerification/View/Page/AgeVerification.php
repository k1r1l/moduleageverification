<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TrueMachine\AgeVerification\View\Page;

/**
 * Contact us widget
 *
 * @ListChild (list="layout.header", zone="customer")
 */
class AgeVerification extends \XLite\View\AView
{
    /**
     * Return list of allowed targets
     *
     * @return array
     */
    public static function getAllowedTargets()
    {
        return array_merge(parent::getAllowedTargets(), 'age_verification');
    }

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/TrueMachine/AgeVerification/page/age_verification/body.twig';
    }

    public function getCSSFiles()
    {
        return array_merge(
            parent::getCSSFiles(),
            array(
                'modules/TrueMachine/AgeVerification/popup.css',
            )
        );
    }

    public function getJSFiles()
    {
        return array_merge(
            parent::getJSFiles(),
            array(
                'modules/TrueMachine/AgeVerification/popup.js',
            )
        );
    }

    public function getTitle()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_title;
    }

    public function getDescription()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_description;
    }

    public function getDenyMessage()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_d_message;
    }

    public function getURLAway()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_url_away;
    }

    public function getSuccessButtonText()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_succ_btn;
    }

    public function getFailureButtonText()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_fail_btn;
    }

    public function getAwayButtonText()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_away_btn;
    }

    public function getWidth()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_width;
    }

    public function getHeight()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_height;
    }

    public function getBackgroundColor()
    {
        return '#' . \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_b_color;
    }

    public function getBackgroundOpacity()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_b_opacity * 0.01;
    }

    public function getPopupType()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_p_type;
    }

    public function getMinAge()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_min_age;
    }

    public function getPopupIsEnabled()
    {
        return \XLite\Core\Config::getInstance()->TrueMachine->AgeVerification->a_v_p_enabled;
    }

    public function getAgeVerification()
    {
        $profile = $this->getProfile();

        \XLite\Core\Session::getInstance()->age_verification =
            \XLite\Core\Session::getInstance()->age_verification == null ? !$this->isAnonymous() ?
                $profile->getAgeVerification() : null : \XLite\Core\Session::getInstance()->age_verification;

        return \XLite\Core\Session::getInstance()->age_verification;
    }

    public function getIsShowPopup()
    {
        return \XLite\Core\Session::getInstance()->age_verification;
    }

    public function isAdmin()
    {
        return \XLite\Core\Auth::getInstance()->isOperatingAsUserMode() ||
            \XLite\Core\Auth::getInstance()->hasRootAccess();
    }

    public function isAnonymous()
    {
        $profile = \XLite\Core\Auth::getInstance()->getProfile();

        return $profile ? $profile->getAnonymous() : true;
    }

    public function getProfile()
    {
        return \XLite\Core\Auth::getInstance()->getProfile();
    }

}
